package recorder;

public class Frame {

    private String mData;

    public Frame(String data) {
        mData = data;
    }

    public String getData() {
        return mData;
    }

}
